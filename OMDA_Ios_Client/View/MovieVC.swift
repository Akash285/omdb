//
//  ViewController.swift
//  OMDA_Ios_Client
//
//  Created by a3logics on 06/12/18.
//  Copyright © 2018 Akash. All rights reserved.
//

import UIKit

class MovieVC: UIViewController {

    @IBOutlet weak var collectionMoviePage: UICollectionView!
    var arrMovieData = Array<Any>()
    var arrPagingData = Array<Any>()
    var fromIndex = 1
    var isPaging = false
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MovieVC.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = self.collectionMoviePage.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumInteritemSpacing = 5
        
        getMovieData(intValue: fromIndex)
        
        //Pull To Refresh
        collectionMoviePage.addSubview(refreshControl)

    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        DispatchQueue.global().async
            {
                self.fromIndex += 1
                self.getMovieData(intValue: self.fromIndex)
                
        }
        refreshControl.endRefreshing()
    }
    
    func getMovieData(intValue:Int)
    {
        let parameters = ["s":"Batman","page":String(fromIndex),"apikey":"eeefc96f"] as Dictionary<String,Any>
        
        ApiCaller.getDataMovie(apiUrl: AppUrl.uploadMasterUrl, strMethodType: AppUrl.get, params: parameters as! [String : String], completion:{ response in
            
            
            DispatchQueue.main.async() { () -> Void in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if (response["Response"] != nil)
                {
                    let statusCode : String = response["Response"] as! String
                    
                    if statusCode == "True"
                    {
                            if let array = response["Search"] as? Array<Any>
                            {
                                self.arrPagingData += array
                                self.arrMovieData.removeAll()
                                self.arrMovieData.append(DataModel.init(dataArray: self.arrPagingData as! Array<Dictionary<String, Any>>))
                                self.collectionMoviePage.reloadData()

                            }
                        
                    }
                    else
                    {
                        self.isPaging = true
                        ApiCaller.showAlertView(strAlertText: response["Error"] as! String)
                    }
                    
                    
                }
                else
                {
                    ApiCaller.showAlertView(strAlertText: "Unkown Error.")
                }
                
                
                
            }
            
            
        })
    }
}

extension MovieVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if arrMovieData.count != 0
        {
            return (arrMovieData[section] as! DataModel).arrModelData.count
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! OMDBCollectionViewCell
        let objValue = (arrMovieData[indexPath.section] as! DataModel).arrModelData[indexPath.item] as! MovieModel
        cell.configureCell(objValue)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let objValue = (arrMovieData[indexPath.section] as! DataModel).arrModelData[indexPath.item] as! MovieModel
        let desController = AppStoryboard.Main.viewControllers(viewControllerClass: DescriptionVC.self)  as! DescriptionVC
        desController.passObjectData = objValue
        self.navigationController?.pushViewController(desController, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.size.width-20)/2, height: collectionView.frame.size.height/3)
        
    }
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionMoviePage.collectionViewLayout.invalidateLayout()
    }
}


