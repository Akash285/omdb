//
//  DescriptionVC.swift
//  OMDA_Ios_Client
//
//  Created by a3logics on 07/12/18.
//  Copyright © 2018 Akash. All rights reserved.
//

import UIKit

class DescriptionVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var passObjectData : MovieModel?
    lazy var lazyImage:LazyImage = LazyImage()
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Description"
        // Do any additional setup after loading the view, typically from a nib.
        print(passObjectData?.titleMovie as Any)
        lblTitleName.text = passObjectData!.titleMovie
        self.lazyImage.showWithSpinner(imageView: logoImageView, url: passObjectData?.imgUrlMovie){
            
            (error:LazyImageError?) in
        }
        self.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension DescriptionVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.font = UIFont.init(name: "Charter-Black", size: 20)
        cell.textLabel!.textAlignment = NSTextAlignment.center;

        if indexPath.row == 0
        {
            cell.textLabel!.text = passObjectData?.typeMovie

        }
        else
        {
            let strDate = passObjectData?.yearMovie
            let date = ApiCaller.getYearAgo(strYear: strDate!)
            cell.textLabel!.text = date.getElapsedInterval()

        }
        return cell
    }
}



