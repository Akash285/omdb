//
//  OMDBCollectionViewCell.swift
//  OMDA_Ios_Client
//
//  Created by a3logics on 06/12/18.
//  Copyright © 2018 Akash. All rights reserved.
//

import UIKit

class OMDBCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var lblYearAgo: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var imgMovie: UIImageView!
    var objMovie: MovieModel!
    lazy var lazyImage:LazyImage = LazyImage()

    func configureCell(_ sliderM: MovieModel)
    {
        self.objMovie = sliderM
        let strDate = objMovie.yearMovie
        let date = ApiCaller.getYearAgo(strYear: strDate)
        lblYearAgo.text = date.getElapsedInterval()
        lblType.text = objMovie.typeMovie
        lblTitleName.text = objMovie.titleMovie
        self.lazyImage.showWithSpinner(imageView: imgMovie, url: objMovie.imgUrlMovie){
            
            (error:LazyImageError?) in
        }
        
        
    }
    
}

extension Date {
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day" :
                "\(day)" + " " + "days ago"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute" :
                "\(minute)" + " " + "minutes ago"
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second" :
                "\(second)" + " " + "seconds ago"
        } else {
            return "a moment ago"
        }
        
    }
}
