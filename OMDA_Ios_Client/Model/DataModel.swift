//
//  DataModel.swift
//  OMDA_Ios_Client
//
//  Created by a3logics on 06/12/18.
//  Copyright © 2018 Akash. All rights reserved.
//

import Foundation

class DataModel:NSObject {
    var arrModelData = Array<Any>()
    var typeMovie = String()
    var yearMovie = String()
    var titleMovie = String()
    var imgUrlMovie = String()
    var ombdId = String()

    init(dataArray: Array<Dictionary<String,Any>>) {
        for movie in dataArray {
            arrModelData.append(MovieModel(movie))
        }
    }
}

class MovieModel: NSObject
{
    var typeMovie = String()
    var yearMovie = String()
    var titleMovie = String()
    var imgUrlMovie = String()
    var ombdId = String()
    
    init(_ dataDic: Dictionary<String,Any>) {
        typeMovie = dataDic["Type"] as? String ?? ""
        yearMovie = dataDic["Year"] as? String ?? ""
        titleMovie = dataDic["Title"] as? String ?? ""
        imgUrlMovie = dataDic["Poster"] as? String ?? ""
        ombdId = dataDic["imdbID"] as? String ?? ""

    }
}
