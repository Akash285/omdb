//
//  AppUrl.swift
//  animation1
//
//  Created by Akash Sharma on 03/04/18.
//  Copyright © 2018 Akash Sharma. All rights reserved.
//

import Foundation

struct AppUrl {
    
    static let uploadMasterUrl = "http://www.omdbapi.com/?"
    
    static let post = "POST"
    static let get  = "GET"
    
}



