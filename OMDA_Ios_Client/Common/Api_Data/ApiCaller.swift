//
//  ApiCaller.swift
//  animation1
//
//  Created by Akash Sharma on 29/03/18.
//  Copyright © 2018 Akash Sharma. All rights reserved.
//

import UIKit

class ApiCaller: NSObject
{
    
    // Send Paramters using this method
    
    static func getDataMovie(apiUrl : String,strMethodType : String,params:[String : String],completion: @escaping (_ success: [String : AnyObject]) -> Void)
    {
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        if reachability.isReachable()
        {
            
            let urlComp = NSURLComponents(string: apiUrl)!

            var items = [URLQueryItem]()
            
            for (key,value) in params {
                items.append(URLQueryItem(name: key, value: value))
            }
            
            items = items.filter{!$0.name.isEmpty}
            
            if !items.isEmpty {
                urlComp.queryItems = items
            }
            
            let session = URLSession.shared
            var dictionary = [String : AnyObject]()
            
            let request = NSMutableURLRequest(url: urlComp.url!)
            request.httpMethod = strMethodType
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let error = error {
                    print(error)
                }
                
                if let response = response
                {
                    let httpResponse = response as! HTTPURLResponse
                    if httpResponse.statusCode == 200
                    {
                        
                        let outputStr  = String(data: data!, encoding: String.Encoding.utf8) as String?
                        let jsonData = outputStr?.data(using: .utf8)
                        dictionary = try! JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! [String : AnyObject]
                        //send this block to required place
                    }
                    else
                    {
                        
                    }
                }
                completion(dictionary)
                
            })
            
            task.resume()
            
        }
        else
        {
            showAlertView(strAlertText: "No Internet Connection")
        }
        
    }
    
    
    static func getYearAgo(strYear:String)-> Date
    {
        let lastYear = String(strYear.suffix(4))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let date = dateFormatter.date(from:lastYear)!
        return date
    }
    
    static func showAlertView(strAlertText:String)
    {
        
        DispatchQueue.main.async() { () -> Void in
            
            // continue with program by calling next step on main thread
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "OMDA"
            alertView.message = strAlertText
            alertView.addButton(withTitle: "OK")
            alertView.show()
            
        }
        
    }
    
    
    
    
    
    
    
}









