//
//  AppStoryboard.swift
//  TeemWurk
//
//  Created by a3logics on 06/09/18.
//  Copyright © 2018 TeemWurk. All rights reserved.
//

import UIKit

enum AppStoryboard : String
{
    case Main = "Main"

    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewControllers(viewControllerClass : UIViewController.Type) -> UIViewController
    {
        return self.instance.instantiateViewController(withIdentifier: viewControllerClass.storyboardID)
    }
    
}

extension UIViewController {
    class var storyboardID : String {
        return "\(self)"
    }
}


