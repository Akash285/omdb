//
//  OMDA_Ios_ClientTests.swift
//  OMDA_Ios_ClientTests
//
//  Created by a3logics on 06/12/18.
//  Copyright © 2018 Akash. All rights reserved.
//

import XCTest
@testable import OMDA_Ios_Client
class OMDA_Ios_ClientTests: XCTestCase {

    
    func testYearAgoValue()
    {
        let objFirstYear = "1992-1995"
        let lastYear = "1995"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let dateAnswer = dateFormatter.date(from:lastYear)!
        XCTAssertEqual(ApiCaller.getYearAgo(strYear: objFirstYear), dateAnswer)
    }
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
